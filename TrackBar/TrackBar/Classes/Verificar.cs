﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrackBar.Classes
{
    class Verificar
    {
        public static int Sobrepasar(int a, int b)
        {
            if(a > 15 && b > 15)
            {
                return 1;
            } else
            {
                return 0;
            }
        }
    }
}
