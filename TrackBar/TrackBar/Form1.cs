﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackBar.Classes;

namespace TrackBar
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            trackBar1.Maximum = 30;
            trackBar2.Maximum = 30;
            
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            
            if(Verificar.Sobrepasar(trackBar1.Value, trackBar2.Value) == 1)
            {
                label1.Text = "warning";
            } else
            {
                label1.Text = "no warning";

            }
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            if (Verificar.Sobrepasar(trackBar1.Value, trackBar2.Value) == 1)
            {
                label1.Text = "warning";
            } else
            {
                label1.Text = "no warning";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }
    }
}
