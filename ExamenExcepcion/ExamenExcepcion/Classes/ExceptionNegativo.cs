﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenExcepcion.Classes
{
    class ExceptionNegativo : Exception
    {
        string mensaje = "Numero negativo no validos";
        public ExceptionNegativo()
        {

        }
        public ExceptionNegativo(string mensaje) : base(mensaje)
        {
        }
        public ExceptionNegativo(string mensaje, Exception innerE):base(mensaje, innerE)
        {

        }

        public string Mensaje { get => mensaje; set => mensaje = value; }
    }
}
