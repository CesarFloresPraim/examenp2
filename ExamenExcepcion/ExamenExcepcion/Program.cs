﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExamenExcepcion.Classes;

namespace ExamenExcepcion
{
    class Program
    {
        static void Main(string[] args)
        {
            string edad;
            int edadN=0;
            Console.WriteLine("Nombre de persona?");
            string nombre = Console.ReadLine();
            Console.WriteLine("Edad?");
            edad = Console.ReadLine();
            edadN = int.Parse(edad);
            
            try
            {
                if (edadN < 0)
                {
                    throw new ExceptionNegativo();
                }
            }
            catch (ExceptionNegativo e)
            {
                Console.WriteLine(e.Mensaje);
                Console.WriteLine(e.Message);

            }

        }
    }
}
