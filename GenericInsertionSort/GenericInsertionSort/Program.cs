﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericInsertionSort.Classes;

namespace GenericInsertionSort
{
    class Program
    {
        static void Main(string[] args)
        {
            float[] arr = { 15, 4.3f, 20, 10.12f, 3, 6, 1 };
            //Rectanguares[] arr = { new Rectanguares(1, 5), new Rectanguares(4, 4), new Rectanguares(2, 2) };
            InsertionSortG.Sort(arr);
            Console.Write("\n\n Los numero ordenados son:\n\n");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write("Elemento [" + (i + 1).ToString()+ "] en arr = ");
                Console.Write(arr[i]);
                Console.Write("\n");
            }

        }
        class Rectanguares : IComparable
        {
            int x;
            int y;
            public Rectanguares(int a, int b)
            {
                x = a;
                y = b;
            }

            public int CompareTo(object obj)
            {
                if (((Rectanguares)obj).x + ((Rectanguares)obj).y > (x + y))
                {
                    return -1;
                }
                else if (((Rectanguares)obj).x + ((Rectanguares)obj).y == (x + y))
                {
                    return 0;
                }
                else return 1;
            }
            public override string ToString()
            {
                return "x:" + x + "y:" + y;
            }
        }
    }
}
